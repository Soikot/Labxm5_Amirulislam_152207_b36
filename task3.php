<?php
class Calculator
{
    public $num1;
    public $num2;

    public function __construct($var1,$var2){
        $this->num1=$var1;
        $this->num2=$var2;
    }
    public function add()
    {
        $addition=$this->num1+$this->num2;
        echo $addition."<br>";

    }
    public function mul()
    {
        $multiplication=$this->num1*$this->num2;
        echo $multiplication."<br>";

    }
    public function div()
    {
        $division=$this->num1/$this->num2;
        echo $division."<br>";

    }
    public function sub()
    {
        $subtraction=$this->num1-$this->num2;
        echo $subtraction."<br>";

    }
}
$mycalc=new Calculator(20,3);
$mycalc->add();
$mycalc->div();
$mycalc->mul();
$mycalc->sub();
